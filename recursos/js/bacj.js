var traductor = (function() {
var key_api2 = "AIzaSyBY0aXaxLFW44mcEPxrYJ4yqP5hXe6pF9Y";
var key_api = "AIzaSyBvjL4T9IvxBw5XETMvoLalX8cTv1ncpwo";
var URL_T = "https://translation.googleapis.com/language/translate/v2?key="+key_api2;
var URL_L = "https://translation.googleapis.com/language/translate/v2/languages?key="+key_api;
var datos;
var traducido;
var map;
var marcadores=[];
var infos=[];
var ar = [];
var ifr =[];

  var _crearJson = function(q, target){
    var json = '{' +
    ' "q": "'+q+'", ' +
	' "target": "'+target+'", ' +
	' "format": "text" ' +
    '}';
    return json;
  }
  
  function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: {lat: 1, lng: 1}
        });	
  }	
  
	var ponerMarcador =  function(obj_localizacion, ifr){
		for(var i = 0; i < obj_localizacion.length; i ++){
			var marker = new google.maps.Marker({
				position: {lat: obj_localizacion[i].latitude, lng: obj_localizacion[i].longitude},
				map: map
			});
			console.log(ifr[i]);
			var infowindow = new google.maps.InfoWindow({
			content: ifr[i],
			position: {lat: obj_localizacion[i].latitude, lng: obj_localizacion[i].longitude},
			//map: map
			});
			marker.addListener('click', function() {
			infowindow.open(map, marker);
			});

		}
	}

    var _hacer_Busqueda = function (){
    var bot_traducir = document.getElementById("traduce");
    bot_traducir.addEventListener("click", function(obj_evento){
		var texto = document.getElementById("texto");
		var traducido_a= document.getElementById("destino");
		var tipo = texto.value;
		var valor = traducido_a.value;
		if(valor === ""){
			//_solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&q="+tipo+"&key="+key_api);
			_solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=snippet&q="+tipo+"&key="+key_api);
		}
		else{
			var json = _crearJson(tipo, traducido_a.value)
			console.log(json);
			_solicitud(json);
		}
	},false);
  }
  
   var _solicitud = function(json){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {   
              var obj = JSON.parse(this.responseText);
			  console.log(obj);
			  var texto = document.getElementById("texto");
			  var tipo = obj.data.translations[0].translatedText;
              //texto.value = obj.data.translations[0].translatedText;
			  //_solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=id&q="+tipo+"&maxResults=50&key="+key_api, tipo);
			  _solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=id&q="+tipo+"&key="+key_api);
              
				console.log(tipo)
			} else {
            }
        }
      };
      xhr.open('POST', URL_T, true);
      xhr.send(json);
  };
  
  var _solicitud_BuscarYoutube = function(url, tipo){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {   
              var obj = JSON.parse(this.responseText);
			  //var texto = document.getElementById("texto");            
				console.log(obj);
				var ids ="";
				for(var i= 0; i < obj.items.length; i++){
					if(i === obj.items.length-1){
						ids += obj.items[i].id.videoId;
					}else{
						ids += obj.items[i].id.videoId+",";
					}
				}
				_solicitud_video(ids, obj, tipo)
				//if(obj.nextPageToken ==! undefined){
				 // _solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=id&q="+tipo+"&maxResults=50&pageToken="+obj.nextPageToken+"&key="+key_api, tipo);
				//}
			} else {
				console.log("error")
            }
        }
      };
      xhr.open('GET', url, true);
	  xhr.send();
  };
  
  var _solicitud_video = function(id, obj_search, tipo){
	  var url = "https://www.googleapis.com/youtube/v3/videos?part=player,snippet,recordingDetails&id="+id+"&key="+key_api;
	   var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {   
              var obj = JSON.parse(this.responseText);
			  //console.log(obj);         
				var v = document.getElementById("ejemplo");
				var a = ""
				for(var i=0; i < obj.items.length; i++){
					if(obj.items[i].recordingDetails === undefined){
					}
					else{
						//console.log(obj.items[i]);
						var x = obj.items[i].recordingDetails.location;
						//.log(x.length);
						if(x !== undefined){
							//console.log(obj.items[i]);
							if(typeof x.latitude !== 'undefined' || typeof x.longitude !== 'undefined' ){
								ifr.push(obj.items[i].player.embedHtml);
								//console.log(obj.items[i].recordingDetails);
								ar.push(x);
								//console.log(ar);
							}
							console.log(ar);
							console.log(ifr);
								console.log("no tiene latitud ni longitud")
						}else{
							console.log("no tiene localizacion")
						}
						
					}	
				}
				v.innerHTML = a;
				console.log(marcadores);
				if(obj_search.nextPageToken !== undefined){
				  _solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=id&q="+tipo+"&maxResults=50&pageToken="+obj_search.nextPageToken+"&key="+key_api);
				}
				//Eventos(marcadores);
				ponerMarcador(ar, ifr);
			} else {
				console.log("error")
            }
        }
      };
      xhr.open('GET', url, true);
	  xhr.send();
  }
  
  var _solicitud_l = function(json){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {   	  
              var obj = JSON.parse(this.responseText);
			  var destino = document.getElementById("destino");
			  console.log(obj.data.languages[0].name)
			  destino.options[0]=new Option("","");
			  for(var  i = 1; i <= obj.data.languages.length; i++){
				  destino.options[i] = new Option(obj.data.languages[i-1].name,obj.data.languages[i-1].language);
			  }
            } else {
				console.log("error");
            }
        }
      };
      xhr.open('POST', URL_L, true);
      xhr.send(json);
  };
  
  var _obtener_len = function(){
	  var s_json = '{'
	  +'"target": "es"' 
	   +'}';
	  _solicitud_l(s_json);
	  
  }
  
   var _traducir = function(){
              _solicitud(_crearJson());
  };
	var _obtener_lenguajes = function(){
		_obtener_len();
		_main();
	};
	
	var _main = function() {
    _hacer_Busqueda();
	initMap();

    
  };
    return {
		"buscar_youtube": _solicitud_BuscarYoutube,
        "traducir": _traducir,
		"obtener_lenguajes": _obtener_lenguajes
    };
} )();
