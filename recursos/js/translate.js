var traductor = (function() {
var key_api2 = "AIzaSyBY0aXaxLFW44mcEPxrYJ4yqP5hXe6pF9Y";
var key_api = "AIzaSyBvjL4T9IvxBw5XETMvoLalX8cTv1ncpwo";
var URL_T = "https://translation.googleapis.com/language/translate/v2?key="+key_api2;
var URL_L = "https://translation.googleapis.com/language/translate/v2/languages?key="+key_api;
var datos;
var traducido;
var map;
var marcadores=[];
var infos=[];
var ar = [];
var ifr =[];
var ids = [];
var contador = 0;
var cvid =0;
var a;
var idsm=[];
var solicitados=[];
var contador_pagina = 0;

  var _crearJson = function(q, target){
    var json = '{' +
    ' "q": "'+q+'", ' +
	' "target": "'+target+'", ' +
	' "format": "text" ' +
    '}';
    return json;
  }
  
  function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: {lat: 1, lng: 1}
        });	
  }	
  
	var ponerMarcador =  function(obj_localizacion, ifr){
			var marker = new google.maps.Marker({
				position: {lat: obj_localizacion.latitude, lng: obj_localizacion.longitude},
				map: map
			});
			var infowindow = new google.maps.InfoWindow({
			content: ifr,
			position: {lat: obj_localizacion.latitude, lng: obj_localizacion.longitude},
			//map: map
			});
			marker.addListener('click', function() {
			infowindow.open(map, marker);
			});
		
	}

    var _hacer_Busqueda = function (){
    var bot_traducir = document.getElementById("traduce");
    bot_traducir.addEventListener("click", function(obj_evento){
		contador = 0;
		cvid = 0;
		contador_pagina=0;
		ids = [];
		solicitados =[];
		initMap();
		var texto = document.getElementById("texto");
		var traducido_a= document.getElementById("destino");
		var tipo = texto.value;
		var valor = traducido_a.value;
		if(valor === ""){
			//_solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&q="+tipo+"&key="+key_api);
			obtener_id(contador, "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&q="+tipo+"&key="+key_api,"");
		}
		else{
			var json = _crearJson(tipo, traducido_a.value)
			console.log(json);
			_solicitud(json);
		}
		     console.log("hemos termindo");
	},false);
  }
  
   var _solicitud = function(json){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {   
              var obj = JSON.parse(this.responseText);
			  console.log(obj);
			  var texto = document.getElementById("texto");
			  var tipo = obj.data.translations[0].translatedText;
              //texto.value = obj.data.translations[0].translatedText;
			  //_solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=id&q="+tipo+"&maxResults=50&key="+key_api, tipo);
			  //_solicitud_BuscarYoutube("https://www.googleapis.com/youtube/v3/search?part=id&type=video&q="+tipo+"&key="+key_api);
              obtener_id(contador, "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&q="+tipo+"&key="+key_api,"");
				console.log(tipo)
			} else {
            }
        }
      };
      xhr.open('POST', URL_T, true);
      xhr.send(json);
  };
  
  var _solicitud_BuscarYoutube = function(url, url_b){
		   var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
			if (xhr.readyState === 4) {
			if (xhr.status === 200) {   
              var obj = JSON.parse(this.responseText);
				console.log(obj);
				for(var i= 0; i < obj.items.length; i++){
					ids.push(obj.items[i].id.videoId);
				}
				contador++;
				if(obj.nextPageToken === undefined){
					contador = 20;
				}
				obtener_id(contador, url_b, obj.nextPageToken);
			} else {
				console.log("error")
            }
        }
      };
      xhr.open('GET', url, true);
	  xhr.send();
  };
  
  	var _solicitud_video = function(id, nvid){
		var url = "https://www.googleapis.com/youtube/v3/videos?part=player,snippet,recordingDetails&id="+id+"&key="+key_api;
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
			if (xhr.status === 200) {  
			  var div = document.getElementById("ejemplo");
			  console.log(div);
              var obj = JSON.parse(this.responseText);
			  var arr = "";
			  console.log(obj);         
				for (var i = 0; i < obj.items.length; i ++){
					console.log(obj.items[i]);
					if(obj.items[i].recordingDetails !== undefined){
						//console.log(obj.items[i]);
						if(obj.items[i].recordingDetails.location !== undefined){
							if(obj.items[i].recordingDetails.location.latitude === undefined || obj.items[i].recordingDetails.location.latitude === undefined){
								arr += obj.items[i].player.embedHtml;
							}else{
								ponerMarcador(obj.items[i].recordingDetails.location, obj.items[i].player.embedHtml)
								arr += obj.items[i].player.embedHtml;
							}
						}else{
							if(obj.items[i].player === undefined){
								if(i > 0){ arr += obj.items[i-1].player.embedHtml}
								else{ arr += obj.items[i+1].player.embedHtml}
								}else{arr += obj.items[i].player.embedHtml;}
							}
					}else{
						if(obj.items[i].player === undefined){
								if(i > 0){ arr += obj.items[i-1].player.embedHtml}
								else{ arr += obj.items[i+1].player.embedHtml}
								}else{arr += obj.items[i].player.embedHtml;}
					}
					//arr += obj.items[i].player.embedHtml;
				}
				solicitados.push(arr);	
				div.innerHTML = arr;
				div2= div.children;
				
					for(var i = 0; i < div2.length; i++){
					div2[i].setAttribute('width', 250);
					div2[i].setAttribute('height', 150);		
					}
				console.log(div2);
			}
				else {
				console.log("error")
            }
			} 
        }
      
      xhr.open('GET', url, true);
	  xhr.send();
	}
	
	var avanzar = function(){
		var boton_avanzar = document.getElementById("siguiente");
		boton_avanzar.addEventListener("click", function(obj_evento){
			if(boton_avanzar !== undefined){
				if(contador_pagina < solicitados.length-1){
					var elemento = document.getElementById("ejemplo");
					while (elemento.firstChild) {
					elemento.removeChild(elemento.firstChild);
					}
					console.log(solicitados[contador_pagina+1]);
					elemento.innerHTML = solicitados[contador_pagina +1]	
					contador_pagina++;
					div2= elemento.children;
					console.log(div2)
					for(var i = 0; i < div2.length; i++){
					div2[i].setAttribute('width', 250);
					div2[i].setAttribute('height', 150);	
					}
				}else{
					var n_busqueda = document.getElementById("nvideos").value;
					var ides="";
					var videos_restantes = (n_busqueda-cvid);
					if(videos_restantes === 0){}
					else{
						if (videos_restantes > 10){
						for (var i = 0; i < 10; i++){
						if( i === 10-1){
						ides += ids[cvid];
					}else{
						ides += ids[cvid]+",";
					}
					cvid++;
					}
					contador_pagina++;
					_solicitud_video(ides, videos_restantes);
					}else{
					for (var i = 0; i < videos_restantes; i++){
					if( i === videos_restantes-1){
						ides += ids[cvid];
					}else{
						ides += ids[cvid]+",";
					}
					cvid++;
					}
					contador_pagina++;
					_solicitud_video(ides, videos_restantes);
					}
					}					
				}
			}
				
		}
		,false);
		
	}
	
	var regresar = function(){
		var regresa = document.getElementById("atras");
		regresa.addEventListener("click", function(obj_evento){
			if(contador_pagina === 0){
		}else{
			var elemento = document.getElementById("ejemplo");
			while (elemento.firstChild) {
			elemento.removeChild(elemento.firstChild);
			}
			console.log(solicitados[contador_pagina-1]);
			elemento.innerHTML = solicitados[contador_pagina - 1]	
			contador_pagina = contador_pagina -1;
			div2= elemento.children;
			console.log(div2)
			for(var i = 0; i < div2.length; i++){
			div2[i].setAttribute('width', 250);
			div2[i].setAttribute('height', 150);	
				}
			}	
		}
		,false);
	}
	
  var obtener_id = function(cont, url, pag_sig){
	 if (cont < 20){
		 if(pag_sig === ''){
			 _solicitud_BuscarYoutube(url, url);
		 }else{
			 _solicitud_BuscarYoutube(url+"&pageToken="+pag_sig, url);
		 }	
	}else{
		console.log(ids);
		var d = document.getElementById("nvideos");
		var nvideo = d.value;
		var ides="";
		if(nvideo > 10){
			for (var i = 0; i < (nvideo-(nvideo-10)); i++){
				if( i === (nvideo -(nvideo-10))){
					ides += ids[i];
				}else{
					ides += ids[i]+",";
				}
				cvid++;
			}
		}else{
			for (var i = 0; i < nvideo; i++){
			if( i === nvideo-1){
				ides += ids[i];
			}else{
				ides += ids[i]+",";
			}
			cvid++;
		}
		}
		_solicitud_video(ides, nvideo);
	}
}
	
  
  var _solicitud_l = function(json){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {   	  
              var obj = JSON.parse(this.responseText);
			  var destino = document.getElementById("destino");
			  console.log(obj.data.languages[0].name)
			  destino.options[0]=new Option("","");
			  for(var  i = 1; i <= obj.data.languages.length; i++){
				  destino.options[i] = new Option(obj.data.languages[i-1].name,obj.data.languages[i-1].language);
			  }
            } else {
				console.log("error");
            }
        }
      };
      xhr.open('POST', URL_L, true);
      xhr.send(json);
  };
  
  var _obtener_len = function(){
	  var s_json = '{'
	  +'"target": "es"' 
	   +'}';
	  _solicitud_l(s_json);
	  
  }
  
   var _traducir = function(){
              _solicitud(_crearJson());
  };
	var _obtener_lenguajes = function(){
		_obtener_len();
		_main();
	};
	
	var _main = function() {
    _hacer_Busqueda();
	initMap();
	avanzar();
	regresar();
  };
    return {
		"buscar_youtube": _solicitud_BuscarYoutube,
        "traducir": _traducir,
		"obtener_lenguajes": _obtener_lenguajes
    };
} )();
