var vision = (function() {
  var key_api = "AIzaSyBY0aXaxLFW44mcEPxrYJ4yqP5hXe6pF9Y";
  var URL_Vision = "https://vision.googleapis.com/v1/images:annotate?key="+key_api;
  var data;

  var _encodeImageFileAsURL = function(){
      var filesSelected = document.getElementById("input").files;
      console.log(filesSelected);
      if (filesSelected.length > 0) {
          var fileToLoad = filesSelected[0];
          var fileReader = new FileReader();
          fileReader.onload = function(fileLoadedEvent) {
              var srcData = fileLoadedEvent.target.result; // <--- data: base64
              console.log(srcData);
              var contenedor = document.getElementById("upload_image");
              var txt2 = srcData.split(",");
              var imagen = document.createElement("img");
              imagen.setAttribute("src",srcData);
              imagen.setAttribute("class","pure-img");
              _sendRequest(_createJson(txt2));
              contenedor.appendChild(imagen);
          }
          fileReader.readAsDataURL(fileToLoad);
      }
  };

  var _createJson = function(arr){
    var json = '{' +
    ' "requests": [' +
    ' { ' +
    '   "image": {' +
    '     "content":"' + arr[1] + '"' +
    '   },' +
    '   "features": [' +
    '       {' +
    '         "type": "TEXT_DETECTION",' +
    '       }' +
    '   ]' +
    ' }' +
    ']' +
    '}';
    return json;
  }

  var _sendRequest = function(request_json){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          var archivo_descargado = document.getElementById("response_text");
          var mensaje = document.createElement("p");
          mensaje.textContent = "";
          if (xhr.status === 200) {           
              var myObj = JSON.parse(this.responseText);
              data = myObj;
              mensaje.textContent = myObj.responses[0].fullTextAnnotation.text ;              
            } else {
              mensaje.textContent = "Error " + this.status + " " + this.statusText + " - " + this.responseURL;
            }
          archivo_descargado.appendChild(mensaje);
        }
      };
      xhr.open('POST', URL_Vision, true);
      xhr.send(request_json);
  };

  var _regresa = function(){
      return data;
  };

    return {
        "upload": _encodeImageFileAsURL  ,
        "returnjson": _regresa
    };
} )();
